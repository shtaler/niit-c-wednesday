/*Task 4. Height converter*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>


int main()

{
	int ch;
	int iFHeight, iIHeight;
	float fSmHeight;

	while (1)
	{
		printf("Enter your height in ft and inches, using format xx'xx\", please \n");

		if (scanf("%d\'%d", &iFHeight, &iIHeight) == 2) //�������� ������������ �����
		{
			fSmHeight = 2.54f*(iFHeight * 12 + iIHeight);		// ������� � ���������

			printf("Your height in centimeters is: %0.1f\n.", fSmHeight);

			break;
		}

		else
		{
			puts("Input error!");
			do
				ch = getchar();
			while (ch != '\n'&& ch != EOF);

		}

	}
	return 0;

}
