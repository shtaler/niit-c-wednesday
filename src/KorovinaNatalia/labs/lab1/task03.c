/*Task 3. Degrees/Radians converter*/
#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

int main()

{
	int ch;
	float fAng, fResult;
	char cType, cTypeC;

	while (1)
	{
		printf("Enter angle in degrees (D) or radians (R), using format \"xx.xxD\" or \"xx.xxR\"  please \n");

		if (scanf("%f%c", &fAng, &cType) == 2) //�������� ������������ ��������� ������
		{
			if (cType == 'D' || cType == 'd') // ������� �������� � �������
			{
				fResult = (fAng*3.14f / 180.0f);
				cTypeC = 'R';
			}
			else if (cType == 'R' || cType == 'r')
			{
				fResult = (fAng * 180.0f / M_PI);	// ������� ������ � �������
				cTypeC = 'D';
			}
			else
			{
				puts("Input error!");
				do
					ch = getchar();
				while (ch != '\n'&& ch != EOF); 
				continue;
			}


			printf("The result of converting is %.4f%c\n", fResult, cTypeC); //����� ������

			break;
		}

		else
		{
			puts("Input error!");
			do
				ch = getchar();
			while (ch != '\n'&& ch != EOF); 

		}

	}
	return 0;


}
