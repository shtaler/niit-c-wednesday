/*Task 2. Time Analysis*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void clean ()
{
    int ch;
    do
        ch = getchar();
    while (ch != '\n'&& ch != EOF);
}

int main()

{
	int iHour, iMinute, iSecond, iChoice;
	char cOutput[] = "Have a nice";
	char cAnswers[][30] = { "night","morning","day","evening" };

	while (1)
	{
		printf("Please enter current time, using format \"hh:mm:ss\" \n");

        if ((scanf("%d:%d:%d", &iHour, &iMinute, &iSecond) == 3) &&  //�������� ������������ ��������� ������
            (iMinute >= 0 && iMinute <= 60) &&
            (iSecond >= 0 && iSecond <= 60))
        {
            if ((iHour >= 21 && iHour <= 23) || (iHour >= 0 && iHour < 6))			// ����������� ������� �����
                iChoice = 0;
            else if (iHour >= 6 && iHour < 12)
                iChoice = 1;
            else if (iHour >= 12 && iHour < 18)
                iChoice = 2;
            else if (iHour >= 18 && iHour < 21)
                iChoice = 3;
            else
            {
                puts("Incorrect time!");
                clean();
                continue;
            }

            printf("%s %s! Current time is %02d:%02d:%02d\n", cOutput, cAnswers[iChoice], iHour, iMinute, iSecond);

            break;
        }


        else
        {
            puts("Input error!");
            clean();
        }
            

	}
	return 0;


}
