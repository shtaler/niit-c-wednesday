/*Lab 05.Task 03. �������� ���������, �������������� ��������� ������� �������
������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h> 
#include <time.h>
#define N 4096

void mixLetters(char *str, int lcount, int i) // ������������� ����������
{
    srand(time(0));
    int k, i0, end;
    char tmp;
    i0 = i;
    end = i + lcount - 2;
    for (i;i < end;i++)
    {
        k =  (rand() % (lcount-2));
        tmp = str[i];
        str[i] = str[i0+k];
        str[i0+k] = tmp;
    }
}

int printWord(char *str, int i)              //����� ����� �� �����
{
    while ((str[i]) != ' ' && (str[i]) != 0)
    {
        printf("%c", str[i]);
        i++;
    }

    putchar(' ');
    return 0;
}
int getWords(char *str, char **pstr)            // �������� ������� ���������� �� ������ ����� ����
{
    int i = 0, m = 0, k = 0, inWord = 0, lcount = 0;

    while (str[i])                              //���� ������� ������ �� ����� ����
    {
        if (str[i] != ' ' && inWord == 0)       //������ � �����
        {
            inWord = 1;
            lcount++;                              // ������� ���� � �����
        }

        else if (str[i] != ' ' && inWord == 1) // ��� � ����� 
        {
            lcount++;
        }

        else if (str[i] == ' ' && inWord == 1) //������� �� �����
        {
            inWord = 0;
            mixLetters(str, lcount, i-lcount+1); //���������� �����
            printWord(str, (i - lcount));  //���������� ������������ �����
            lcount = 0;
        }
        i++;
    }
    printf("\n");
    return 0;
}

int main()
{
    FILE *fp;
    char str[N];
    char *pstr[N];

    fp = fopen("test.txt", "rt");
    if (fp == 0)
    {
        perror("File");
        return 1;
    }

    while (fgets(str, N, fp)) 
    {
      getWords(str,pstr);
      memset(str, 0, sizeof(char)*N);  //������� �������
      memset(pstr, 0, sizeof(char*)*N); //������� �������

    }

    fclose(fp);
    return 0;
}
