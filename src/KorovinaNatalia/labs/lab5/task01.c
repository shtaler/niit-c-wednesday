/*Lab05. Task01 *�������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#define N 80

int getWords(char *str, char **pstr)            // �������� ������� ���������� �� ������ ����� ����
{
    int i = 0, m = 0, inWord = 0;

    while (str[i])                              //���� ������� ������ �� ����� ����
    {
        if (str[i] != ' ' && inWord == 0)       //������ � �����
        {
            inWord = 1;
            pstr[m] = &str[i];
            m++;                                //������� ����
        }

        else if (str[i] == ' ' && inWord == 1) //����� �� �����
        {
            inWord = 0;
        }

        i++;
    }
    return m;
}
 void mixWords (char **pstr, int count) // ������������� ����������
 {
     srand(time(0));
     int i, k;
     char *tmp;
     for (i = 0;i < count;i++)
     {
         k = rand() % count;
         tmp = pstr[i];
         pstr[i] = pstr[k];
         pstr[k] = tmp;
     }
 }

 int printWord(char **pstr, int i)              //����� ����� �� �����
 {
     while ((*pstr[i]) != ' ' && (*pstr[i]) != 0)
     {
         printf("%c", *pstr[i]);
         pstr[i]++;
     }

     putchar(' ');
     return 0;
 }

int main()
{
    int i = 0;
    int count = 0;
    char str[N] = {0};
    char *pstr[N] = { 0 };
    printf("Input your string please:\n");
    fgets(str, N, stdin);
    if (str[strlen(str) - 1] == '\n')
        str[strlen(str) - 1] = 0;
    count=getWords(str, pstr); //���������� ������� ���������� + ��������� ���������� ���� � ������
    mixWords(pstr, count); // ������������� ������� ����������
    while (pstr[i])  // ����� ������ �� ������
    {
        printWord(pstr, i); // ����� ����� �� ������
        i++;
    }
    putchar('\n');
    return 0;
}