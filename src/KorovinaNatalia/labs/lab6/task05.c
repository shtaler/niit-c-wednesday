/*Lab 06. Task 05. �������� ���������, ������� �������� ����� ���������� N - ���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������) �� ����� � � ���� */

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define MAX 45

unsigned long long fib(int n)  //������ ���������� � ����������������� ������, 
                               //��� ����� ��������� ������� ������� ����������
{
    if (n == 1 || n == 2)
        return 1;
    else
        return fib(n - 1) + fib(n - 2);
}

int main ()
{
    int n;
    clock_t begin, end;
    double delta;
    FILE *fp;
    fp = fopen("fibonacci.xls", "wt");
    for (n = 1;n < MAX;n++)
    {
        begin = clock();                 
        printf("%d-%llu\n", n, fib(n));
        end=clock();
        delta = ((double)(end - begin)) / CLOCKS_PER_SEC; //����� ����������
        fprintf(fp, "%d\t%.2lf\n", n, delta); //������ ������ � ������� ���������� � ����
     
    }
    fclose(fp);
    return 0;
}