/*Lab 06. Task 02. �������� ���������, ������� ������� � ��������� ����� ����� �� 
2 �� 1000000 �����, ����������� ����� ������� ������������������ ��������*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define MAX 100000
unsigned long long k = 0;

unsigned long long collats (unsigned long long n) // ���������� ������������������ �������� ��� ����� n
{
    if (n % 2 == 0)
    {
        collats(n / 2);
        k++;
    }
    else if (n != 1)
    {
        collats(3 * n + 1);
         k++;
    }
    return 0;
}

void clean()  // �-�� �������
{
    int ch;
    do
        ch = getchar();
    while (ch != '\n'&& ch != EOF);
    printf("Input error! ");
}

int main()
{

    unsigned long long kmax=0;
    int i, imax = 0;
    
    for (i = 2;i <= MAX;i++)   
    {
       collats(i);
        if (k > kmax)
        {
            imax = i;
            kmax = k;
        }
        k = 0;
    }
       
    printf("%d amount %llu \n", imax, kmax);
    return 0;

}