/*Lab 06. Task 04. �������� ���������, ������� ��������� ������ ������������ � ����������� ��������� */

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#define MAX 100
#define MIN 0
typedef unsigned char UC;


unsigned long long linearSum(UC *arr, int size) // �������� ���������������� ��������
{
    int i = 0;
    unsigned long long sum = 0;
    for (i = 0; i < size; i++)
        sum += arr[i];
    return sum;
}

unsigned long long recursionSum(UC *arr, int size) // �-�� ��������� ����� ��������� ������� ����������
{
    if (size == 1)
        return *arr;
    else
        return  recursionSum(arr, size / 2) + recursionSum((arr + size / 2), size - size / 2);
}

void clean()  // �-�� �������
{
    int ch;
    do
        ch = getchar();
    while (ch != '\n'&& ch != EOF);
    printf("Input error! ");
}

int main()
{
   UC *arr;
   int N, M, i=0;
   clock_t begin, end;
   double delta1, delta2;
   unsigned long long sum = 0;
   srand(time(0));

   while (1)
   {
       printf("Input your number:\n");
       if ((scanf("%d", &M)) == 0)
           clean();
       else
           break;
   }
   
   N = (int) pow(2, M);                                      // ������ ������� �����
   printf("The number of elements in the array %d\n", N);
   arr = (UC*)malloc(sizeof(UC)*N);                          //�������� ������������� �������
   if (arr == 0)
       return 1;
   
   for (i = 0; i < N; i++)                                    // ���������� �������  ���������� �������
   {
       arr[i] = (rand() % (MAX - MIN) + 1 + MIN);
   }

   begin = clock();
   printf("The sum, counted using recursion is %llu\n", recursionSum(arr, N));
   end = clock();
   delta1 = ((double)(end - begin)) / CLOCKS_PER_SEC;
   begin = clock();

   printf("The sum, counted using linear method is %llu\n", linearSum(arr, N));
   end = clock();
   delta2 = ((double)(end - begin)) / CLOCKS_PER_SEC;
   printf("Counting time, using recursion is %.3lf sec\nCounting time, using linear method is %.3lf sec\n ",delta1, delta2);
    
   free(arr);
   return 0;

}