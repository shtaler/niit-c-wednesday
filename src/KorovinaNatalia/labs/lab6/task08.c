/*Lab 06. Task 08. ��������� ����������� */
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 256

char partition(char *ip1, char *ip2, char *buf) //�-� ��������� ������
{
    int i, j, balance = 0; //��������, ���������� ����������� ���-�� ������
    char op;
    for (i = 0;i <= strlen(buf);i++)
    {
        if (buf[i] == '(')
        {
            balance += 1;
        }
        else if (buf[i] == ')')
        {
            balance -= 1;
        }
        else if (balance == 1 && (buf[i] == '/' || buf[i] == '*' || buf[i] == '-' || buf[i] == '+')) //���� ������ ���� � ������ ������ ��������
        {
            op = buf[i]; // ������ �����
            for (j = 0; j < i - 1; j++) //������ ������� ��������, ����� �� �����(���������� �������� ������)
                ip1[j] = buf[j + 1];
            j = 0;
            while (buf[((i + 1) + j) + 1]) //������ ������� ��������, ������ �� �����
            {
                ip2[j] = buf[i + 1 + j];
                j++;
            }
            return op;
        }
    }
    return 1;
}

int eval(char *buf) //����������� �-�� ���������� �������� ���������
{
    char ip1[N] = { 0 }, ip2[N] = { 0 }, op; //������ �������, ������ �������, ���� ��������
    op = partition(ip1, ip2, buf);
    if (buf[0] != '(') // ���� ������ ��� - ������� ���������(�����)
        return atoi(buf);
    else 
    {
        switch (op)
        {
        case '+':
            return eval(ip1) + eval(ip2);
        case '-':
            return eval(ip1) - eval(ip2);
        case '/':
            return eval(ip1) / eval(ip2);
        case '*':
            return eval(ip1) * eval(ip2);
        default:
            return 1;
        }
    }
}

int main(int argc, char* argv[])
{
    if (argv[1])
        printf("%d\n", eval(argv[1]));
    else
    {
        printf("Input error!\n");
        exit(1);
    }
    return 0;
}
