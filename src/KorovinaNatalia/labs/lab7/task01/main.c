/*Lab 07. Task 01. �������� ���������, ��������� ��������� ������ � �������� �
�������� � �� ����� � ������������ � ����������� ����� ������ 
��������� ������ ������������ ��������� �������:
(a) ������������ ������ �� ������ ������ �����.
(b) ����� � ����� ���� ������ �� ���������� ����������� ������.
(c) ����� ����������� ������� �� ��������. */

#define _CRT_SECURE_NO_WARNINGS
#define N 256
#include "regions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *fp;
    int count = 0;
    int searchType = 0; // ���������� �������� �� ��� ������ (�� ��� ��� �� ��������)
    char buf[512];
    char eregion[N] = { 0 };
    char eiso[N] = { 0 };
    PITEM head, tail;
    fp = fopen("fips10_4.csv", "rt");
    if (!fp)
    {
        perror("File names.csv");
        exit(1);
    }
    fgets(buf, 512, fp);
    while (fgets(buf, 512, fp))
    {
        if (count == 0)
        {
            head = createList(createRegion(buf));
            tail = head;
        }
        else
        {
            tail = addToTail(tail, createRegion(buf));
        }
        count++;
    }

    fclose(fp);
    puts("Select the type of searching. Enter the number:\n1.Search by region.\n2.Search by ISO.");
    scanf("%d", &searchType);
    switch (searchType)
    {
    case 1:
    {
        puts("Enter a region:");
        scanf("%s", &eregion);
        findByName(head, eregion);
        break;
    }
    case 2:
    {
        puts("Enter an ISO code:");
        scanf("%s", &eiso);
        findByIso(head, eiso);
        break;
    }
    default:
    {
        puts("Input error!");
        break;
    }
    }
    return 0;
}