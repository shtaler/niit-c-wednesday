#include "regions.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

PITEM createList(PREGION region)
{
    PITEM item = (PITEM)malloc(sizeof(TITEM)); // ��������� �� ����� ������������ ������
    item->region = region; 
    item->prev = NULL;
    item->next = NULL;
    return item; // ����� ������ ��������
}

PREGION createRegion(char *line) //�������� ������ � �������, � ���������� ���� ����������
{
    int i = 0;
    char fips[3];
    PREGION rec = (PREGION)malloc(sizeof(TREGION));
    while (*line && *line != ',')
        rec->iso[i++] = *line++;
    rec->iso[i] = 0;
    line++;
    i = 0;
    while (*line  && *line != ',')
        fips[i++]= *line++;
    fips[i] = 0;
    rec->fips = atoi(fips);
    line++;
    i = 0;
    while (*line)
        rec->name[i++] = *line++;
    rec->name[i] = 0;
    return rec;

}

PITEM addToTail(PITEM tail, PREGION name_rec)
{
    PITEM item = createList(name_rec);
    if (tail != NULL)
    {
        tail->next = item;
        item->prev = tail;
    }
    return item;
}

int countList(PITEM head)
{
    int count = 0;
    while (head)
    {
        count++;
        head = head->next;
    }
    return count;
}

void findByName(PITEM head, char *name)  //����� �� ����� �������
{
    while (head)
    {
        if (strstr(head->region->name, name) != 0)
            printf("ISO code: %s\nFIPS code: %2d\nRegion name: %s\n", head->region->iso, head->region->fips, head->region->name);
        head = head->next;
    }
}

void findByIso(PITEM head, char *iso) //����� �� ���
{
    while (head)
    {
        if (strstr(head->region->iso, iso) != 0)
            printf("ISO code: %s\nFIPS code: %2d\nRegion name: %s\n", head->region->iso, head->region->fips, head->region->name);
        head = head->next;
    }
}
