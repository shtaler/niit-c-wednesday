struct REGION
{
    char iso[3];
    int fips;
    char name[256];
};

typedef struct REGION TREGION;
typedef TREGION * PREGION;

struct ITEM
{
    PREGION region;
    struct ITEM *next;
    struct ITEM *prev;
};

typedef struct ITEM TITEM;
typedef TITEM * PITEM;

PITEM createList(PREGION region);
PREGION createRegion(char *line); //�������� ����� ������ � ������. �������� ���� ����� �� �����
PITEM addToTail(PITEM tail, PREGION  county); // ���������� ������ �������� � ����� ������. ���� ����� ����� ���������� �������� ������. � ������, ������ ���������� � ���� �������
int countList(PITEM head); // ��������� ������ - ������� ���-�� ��������� ������. 
void findByName(PITEM head, char *name); //����� �� ����� �������� ������ � ������� ������.
//void printRegion(PITEM item);
void findByIso(PITEM head, char *iso); //����� �� ��� �������� ������ � ������� ������.