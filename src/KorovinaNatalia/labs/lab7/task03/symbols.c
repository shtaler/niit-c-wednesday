#define _CRT_SECURE_NO_WARNINGS
#include "symbols.h"
#include <stdio.h>
#include <stdlib.h>

void  PrintSymbol(const PSYMBOL element) // ����� �� ������
{
    if (element->symbol=='\n')
        printf("Enter - %f\n", element->frequency);
    else if (element->symbol == ' ')
        printf("Space - %f\n", element->frequency);
    else
        printf("%5c - %f\n", element->symbol, element->frequency);
}