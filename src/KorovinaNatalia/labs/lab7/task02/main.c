/*Lab 07. Task 02.�������� ���������, ������� ����������� �������� ��� ����� ��
����� �� � ������� ������� ������������� �������� ���� �����. �������� ����� �������� � ��������� �����. */

#define _CRT_SECURE_NO_WARNINGS
#include "tree.h"
#include <stdio.h>
#include <stdlib.h>
#define N 256
int main(int argc, char**argv)
{
    FILE *fpKey, *fpCode;
    PNODE root = NULL;
    char keyword[N];
    char buf[N];
    if (argc < 2)
    {
        puts("Define filename");
        exit(1);
    }

    fpCode = fopen(argv[1], "rt");
    if (fpCode == NULL)
    {
        perror("File:");
        exit(2);
    }
    fpKey = fopen("Keywords.txt", "rt");
    if (!fpKey)
    {
        perror("Need Keywords.txt");
        exit(3);
    }
    while (fscanf(fpKey, "%s", keyword) != EOF)
        root = AddTree(root, keyword);
    while (fscanf(fpCode, "%s", buf) != EOF)
        SearchTree(root, buf);
    PrintTree(root);
    fclose(fpKey);
    fclose(fpCode);

    return 0;
}