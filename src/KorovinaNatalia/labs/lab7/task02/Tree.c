#define _CRT_SECURE_NO_WARNINGS
#include "tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

PNODE AddTree(TNODE *root, char *keyword) 
{
    if (root == NULL)
    {
        root = (PNODE)malloc(sizeof(TNODE));
        strcpy(root->keyword, keyword);
        root->count = NULL;
        root->left = root->right = NULL;
    }
    else if (_strcmpi(root->keyword, keyword) > 0)
        root->left = AddTree(root->left, keyword);
    else if (_strcmpi(root->keyword, keyword) < 0)
        root->right = AddTree(root->right, keyword);
    return root;
}

void PrintTree(PNODE root) // ������ ������
{
    if (root->left)
        PrintTree(root->left);
    printf("%s-%lu\n", root->keyword, root->count);
    if (root->right)
        PrintTree(root->right);
}

PNODE SearchTree(TNODE *root, char *buf) //����� �� ������
{
    if (_strcmpi(root->keyword, buf) > 0)
        if (root->left)
            SearchTree(root->left, buf);
        else return root;
    else if (_strcmpi(root->keyword, buf) < 0)
        if (root->right)
            SearchTree(root->right, buf);
        else return root;
    else
        root->count++;
    return root;
}

