/* Lab 04. Task 05. �������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 200
#define M 200


int comp(const void *a, const void *b)
{
    return strlen(*(char**)a) - strlen(*(char**)b);
}

int main()
{
    char str[N][M] = { 0 };
    char *pstr[N] = { 0 };
    int count = 0, i;
    FILE *fp, *out;
   

    fp = fopen("TomBombadil.txt", "rt");
    out = fopen("output.txt", "wt"); 

    if (fp == 0 || out == 0)
    {
        perror("File");
        return 1;
    }

    while (count < N && (fgets(str[count], M, fp) != 0))
    {
        pstr[count] = str[count];
        count++;
    }

    qsort(pstr, count, sizeof(char*), comp);

    for (i = 0;i < count;i++)
    {
        fprintf(out, "%s", pstr[i]);
        if (pstr[i][(strlen(pstr[i]))-1] != '\n') 
            fprintf(out, "\n");
    }

    fclose(fp);
    fclose(out);
    return 0;

}
