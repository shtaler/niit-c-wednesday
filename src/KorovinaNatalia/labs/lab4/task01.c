/*Lab 04. Task 01. �������� ���������, ������� ��������� ������������ ������ ������� -
�� ����� � ����������, � ����� ��������� �� � ������� �������� -
��� ����� ������.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define M 256
#define N 100

int comp(const void *a, const void *b)
{
    return strlen(*(char**)a) - strlen(*(char**)b);
}

int main()
{
    char str[N][M];
    char *pstr[N] = { 0 };
    int count = 0, i;
    while (count < N && *fgets(str[count], M, stdin) != '\n')
    {
        pstr[count] = str[count];
        count++;
    }

    qsort(pstr, count, sizeof(char*), comp);

    for (i = 0;i < count;i++)
        printf(pstr[i]);

    return 0;
}
