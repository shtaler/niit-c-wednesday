/*Task1. Altimeter of bomb*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define G 9.81f

int main()
{
	char ch;
	int iHeight; // Height of the bomb at the start
	float h = 0000.0f; //Height of bomb in the current moment
	int t = 0; // Current time
	clock_t now;
	clock_t time0;
	
	while (1)
	{
		printf("Enter the height in meters please: ");
		if (scanf("%d", &iHeight) == 1)
		{			
			time0 = clock();
			while (1)
			{
				now = clock();
				t = (now - time0) / 1000;
				h = iHeight - (G*t*t/ 2);
				if (h > 0)
				{
					
					printf("t=%02dc h=%06.1fm\n", t, h);
					now = clock();
					while (clock() < now + 1000);
				}
				else
				{
					printf("BOOM!\n");
					break;
				}
			}
			break;
		}
		else
		{
			puts("Input error!");
			do
				ch = getchar();
			while (ch != '\n'&& ch != EOF); 

		}

	}

		return 0;
}