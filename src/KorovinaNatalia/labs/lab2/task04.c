/*Task 4. Sorting*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#define ROW 80

int main()
{
	char k;
	int i = 0;
	int j = 0;
	int n = 0;
	char cString[ROW] = { 0 };
		
		printf("Input your row:\n");
		scanf("%s", &cString);
		i = 0; 
		j = strlen(cString) - 1;
		
		while (i<j)

		{
			if (cString[i] >= '0' && cString[i] <= '9')

			{
				while (j > i)
				{
					if ((cString[j] >= 'a' && cString[j] <= 'z') || (cString[j] >= 'A' && cString[j] <= 'Z'))
					{
						k = cString[i];
						cString[i] = cString[j];
						cString[j] = k;
						break;
					
					}
                    j--;
				}
				
			}
			i++;
		}
		
		
		for (n = 0;cString[n]!=0;n++)
			putchar(cString[n]);
		printf("\n");
		

	return 0;


}
