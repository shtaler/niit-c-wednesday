#define _CRT_SECURE_NO_WARNINGS
#include "decompressor.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct SYMBOL* buildTree(struct SYMBOL *psym[], int N)
{
    // ������ ��������� ����
    struct SYMBOL *temp = (struct SYMBOL*)malloc(sizeof(struct SYMBOL));
    // � ���� ������� ������������ ����� ������
    // ���������� � �������������� ��������� ������� psym
    temp->frequency = psym[N - 2]->frequency + psym[N - 1]->frequency;
    // ��������� ��������� ���� � ����� ���������� ������
    temp->left = psym[N - 1];
    temp->right = psym[N - 2];
    temp->code[0] = 0;
    temp->symbol = 0;

    if (N == 2) // �� ������������ �������� ������� � �������� 1.0
        return temp;
    else // ��������� temp � ������ ������� psym, �������� ������� �������� �������
    {
        for (int i = 0;i <N;i++)
            if ((temp->frequency) >(psym[i]->frequency))
            {
                for (int j = N - 1; j > i;j--)
                    psym[j] = psym[j - 1];
                psym[i] = temp;
                break;
            }
        return buildTree(psym, N - 1);
    }
}

void Unpack(char ch, unsigned char *buf)
{
    union CODE code;
    code.ch = ch;
    buf[0] = code.byte.b1 + '0';
    buf[1] = code.byte.b2 + '0';
    buf[2] = code.byte.b3 + '0';
    buf[3] = code.byte.b4 + '0';
    buf[4] = code.byte.b5 + '0';
    buf[5] = code.byte.b6 + '0';
    buf[6] = code.byte.b7 + '0';
    buf[7] = code.byte.b8 + '0';
}

void SearchTree(PSYMBOL root, FILE *fpIn, FILE *fpOutDeComp)
{
    int ch;
    if (root->left || root->right)
    {
        ch = fgetc(fpIn);
        if ((char)ch == '1')
            SearchTree((root->right), fpIn, fpOutDeComp);
        else
            SearchTree((root->left), fpIn, fpOutDeComp);
    }
    else
        fwrite(&root->symbol, sizeof(root->symbol), 1, fpOutDeComp);
}


