#define _CRT_SECURE_NO_WARNINGS
#include "compressor.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 300
int comp(const void *a, const void *b)
{
    if ((((TSYMBOL*)b)->frequency) > (((TSYMBOL*)a)->frequency))
        return 1;
    else 
        return -1 ;
}

int main(int argc, char**argv)
{
    unsigned char buf[8] = { 0 };
    FILE *fpIn, *fpOut, *fpOutComp = NULL;
    int i = 0, j = 0, ch, unicS = 0, count10 = 0;
    float sumFreq = 0;
    unsigned long long count = 0;
    TSYMBOL syms[N] = { 0 };
    PSYMBOL psyms[N] = { 0 };
    PSYMBOL root = { 0 };
    unsigned char out;
    char formatIn[10] = { 0 };
    char nameIn[256] = { 0 };
    char newName[256] = { 0 };
    char formatSign[4] = ("NKC"); //������� �������

    if (argc < 2)
    {
        puts("Define filename");
        exit(1);
    }

    fpIn = fopen(argv[1], "rb");
    if (fpIn == NULL)
    {
        perror("File:");
        exit(1);
    }
    //����������� ������� ��������� �����
    i = 0;
    j = 0;
    while (*(argv[1] + i) != '.')
    {
        nameIn[i] = *(argv[1] + i);
        i++;
    }
    while (*(argv[1] + i + 1) != 0)
    {
        formatIn[j] = *(argv[1] + i + 1);
        i++;
        j++;
    }

    //����������� ������� ��������� �����
    fseek(fpIn, 0, SEEK_END);
    long size = ftell(fpIn);

    rewind(fpIn);

    fpOut = fopen("101.txt", "w+b");
    if (fpOut == NULL)
    {
        perror("File:");
        exit(2);
    }

    while ((ch = fgetc(fpIn)) != EOF)
    {
        count += 1;
        syms[ch].symbol = (char)ch;
        syms[ch].frequency += 1;
    }

    qsort(syms, N, sizeof(TSYMBOL), comp);
    i = 0;
    while (syms[i].frequency != 0)
    {
        psyms[i] = &syms[i];
        syms[i].frequency = syms[i].frequency / count;
        i++;
        unicS += 1;
    }

    root = buildTree(psyms, unicS);
    makeCodes(root);

    rewind(fpIn);
    while ((ch = fgetc(fpIn)) != -1)
    {
        for (i = 0;i < count;i++)
            if (syms[i].symbol == (unsigned char)ch)
            {
                fputs(syms[i].code, fpOut); // ������� ������ � �����
                count10 = count10 + strlen(syms[i].code);
                break; // ��������� �����
            }
    }

    fclose(fpIn);
    fclose(fpOut);

    fpIn = fopen("101.txt", "rt");
    if (fpIn == NULL)
    {
        perror("File:");
        exit(1);
    }
    sprintf(newName, "%s.cnk", nameIn); //��� ������� �����
    fpOut = fopen(newName, "wb"); //������� ������ ����� ��� ������
    if (fpOut == NULL)
    {
        perror("File:");
        exit(2);
    }

    fwrite(&formatSign, sizeof(formatSign), 1, fpOut); //1.������ ������� �������
    fwrite(&unicS, sizeof(unicS), 1, fpOut);//2.������ ���-�� ���������� ��������
    i = 0;  //3. ������ ������� ���� + ������� �������������
    while (syms[i].frequency)
    {
        fwrite(&syms[i].symbol, sizeof(char), 1, fpOut);
        fwrite(&syms[i].frequency, sizeof(float), 1, fpOut);
        i++;
    }

    int tale = count10 % 8; // 4. ������ ������
    fwrite(&tale, sizeof(tale), 1, fpOut);

    fwrite(&size, sizeof(size), 1, fpOut); // 5.������ ������� ��������� �����
    fwrite(&formatIn, sizeof(formatIn), 1, fpOut); // 6. ������ ���������� ��������� �����

    ch = 0;   //������ ������ ������
    while (ch != EOF)
    {
        for (i = 0;i < 8;i++)
        {
            if ((ch = getc(fpIn)) != EOF)
                buf[i] = (unsigned char)ch;
            else if (i > 0)
            {
                for (i;i < 8;i++)
                    buf[i] = '0';
                break;
            }
            else break;
        }
        out = pack(buf);
        fwrite(&out, sizeof(unsigned char), 1, fpOut);
    }
    fclose(fpIn);
    fclose(fpOut);
    if (remove("101.txt") == -1)
    {
        puts("Error of deleting");
        exit(3);
    }
    return 0;
}