struct SYMBOL // ������������� �������
    {
        unsigned char symbol; // ASCII-���
        float frequency; // ������� �������������
        char code[256]; // ������ ��� ������ ����
        struct SYMBOL *left; // ����� ������� � ������
        struct SYMBOL *right; // ������ ������� � ������
    };

union CODE {
    unsigned char ch;
    struct {
        unsigned short b1 : 1;
        unsigned short b2 : 1;
        unsigned short b3 : 1;
        unsigned short b4 : 1;
        unsigned short b5 : 1;
        unsigned short b6 : 1;
        unsigned short b7 : 1;
        unsigned short b8 : 1;
    } byte;
};

typedef struct SYMBOL TSYMBOL;
typedef TSYMBOL* PSYMBOL;

void makeCodes(struct SYMBOL *root);
struct SYMBOL* buildTree(struct SYMBOL *psym[], int N);
unsigned char pack(unsigned char buf[]);